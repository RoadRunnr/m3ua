@copyright 2015-2018 SigScale Global Inc.
@version %VERSION%
@title %DESCRIPTION%
@doc This application implements an M3UA protocol stack.
@reference <a href="http://tools.ietf.org/rfc/rfc2719">RFC2719: Framework Architecture for Signaling Transport</a>
@reference <a href="http://tools.ietf.org/rfc/rfc4166">RFC4166: Telephony Signalling Transport over Stream Control Transmission Protocol (SCTP) Applicability Statement</a>
@reference <a href="http://tools.ietf.org/rfc/rfc4666">RFC4666: Signaling System 7 (SS7) Message Transfer Part 3 (MTP3) - User Adaptation Layer (M3UA)</a>

==Introduction==
The Signaling Transport (SIGTRAN) protocol suite provides for transport
of telephony signaling protocol messages over Internet Protocol (IP) networks.

==Operation==

Create an M3UA Application Server Process (ASP) endpoint:
```
	(as@h1)1> application:start(m3ua).
	ok
	(as@h1)2> {ok, EP} = m3ua:open().  
	{ok,<0.51.0>}
'''
Create an M3UA Signaling Gateway Process (ASP) endpoint:
```
	(sg@h2)1> application:start(m3ua).
	ok
	(sg@h2)2> {ok, EP} = m3ua:open(2905, [{sctp_role, server}, {m3ua_role, sgp}]).
	{ok,<0.51.0>}
'''
Establish an SCTP association from the ASP to SGP:
```
	(as@h1)3> {ok, Assoc} = m3ua:sctp_establish(EP, {127,0,0,1}, 2905, []).
{ok,192}
'''

==Database Schema==
Describe the layout of any mnesia tables used in the application....

==Configuration==
Document all application environment variables which exist...

The application is configured using the following environment variables:
<dl>
  <dt>foo</dt>
    <dd>Describe the effect of the foo variable.</dd>
  <dt>bar</dt>
    <dd>Describe the effect of the bar variable.</dd>
</dl>

